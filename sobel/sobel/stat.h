#ifndef STAT_H
#define STAT_H

#include <algorithm>
#include <cmath>

class STAT{
public:

	static long median(long *time_measurements, unsigned NUM_OF_TRIALS)
	{
		std::sort(time_measurements, time_measurements + NUM_OF_TRIALS);
		return time_measurements[NUM_OF_TRIALS / 2];
	}
	static long mean(long *time_measurements, unsigned NUM_OF_TRIALS)
	{
		return sum(time_measurements, NUM_OF_TRIALS) / NUM_OF_TRIALS;
	}
	static long variance(long *time_measurements, unsigned NUM_OF_TRIALS)
	{
		long mn = mean(time_measurements,  NUM_OF_TRIALS);
		long acc = 0;
		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			acc = acc + (time_measurements[i] - mn)*(time_measurements[i] - mn);
		}
		return acc / (NUM_OF_TRIALS - 1);
	}
	static long sd(long *time_measurements, unsigned NUM_OF_TRIALS)
	{
		return std::sqrt(variance(time_measurements, NUM_OF_TRIALS));
	}
	static long sum(long *time_measurements, unsigned NUM_OF_TRIALS)
	{
		unsigned long accum = 0;
		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			accum = accum + time_measurements[i];
		}
		return accum;
	}
};

#endif