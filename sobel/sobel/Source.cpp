#include "ppm.h"
#include "stat.h"
#include <math.h> 
#include <thread>
#include <chrono>
#include <stdio.h>
#include <stdlib.h>


#define NUM_OF_TRIALS 10


#define NOT_ACTUAL_SOBEL_FILTER(i) ((img_grey->data[i - x - 1] + (img_grey->data[i - x] + img_grey->data[i - x]) + img_grey->data[i - x + 1]				\
	-								 img_grey->data[i + x - 1] - (img_grey->data[i + x] + img_grey->data[i + x]) - img_grey->data[i + x + 1])				\
	+								(img_grey->data[i - x + 1] + (img_grey->data[i + 1] + img_grey->data[i + 1]) + img_grey->data[i + x + 1]				\
	-								 img_grey->data[i - x - 1] - (img_grey->data[i - 1] + img_grey->data[i - 1]) - img_grey->data[i + x - 1]));

#define SOBEL_FILTER(i) sqrt(std::pow((img_grey->data[i - x - 1] + (img_grey->data[i - x] + img_grey->data[i - x]) + img_grey->data[i - x + 1]				\
	-								   img_grey->data[i + x - 1] - (img_grey->data[i + x] + img_grey->data[i + x]) - img_grey->data[i + x + 1]),2)			\
	+						 std::pow((img_grey->data[i - x + 1] + (img_grey->data[i + 1] + img_grey->data[i + 1]) + img_grey->data[i + x + 1]				\
	-								   img_grey->data[i - x - 1] - (img_grey->data[i - 1] + img_grey->data[i - 1]) - img_grey->data[i + x - 1]),2));

// Photoshop, GIMP grayscale:
//		Gray = (Red * 0.3 + Green * 0.59 + Blue * 0.11)
// Original ITU - R recommendation BT.709:
//		Gray = (Red * 0.2126 + Green * 0.7152 + Blue * 0.0722)
// ITU-R Recommendation BT.601:
//		Gray = (Red * 0.299 + Green * 0.587 + Blue * 0.114)

void serial_grey  (PPMImage*, PGMImage*);
void parallel_grey(PPMImage*, PGMImage*);

// Padding options:
//		1. Valid Padding - Do not include edge pixels
//		2. Same Padding  - adding aditional rows and columns to edges and filling with zeros

void serial_sobel(PGMImage*, PGMImage*);
// Scalable solution
void parallel_sobel(PGMImage*, PGMImage*);
// Fixed 4 threads according to requirements
void parallel_sobel_4boxes(PGMImage*, PGMImage*);
void omp_sobel(PGMImage*, PGMImage*);

void batch(unsigned grey_Id = 1, unsigned sobel_Id = 1);

unsigned SCALE_THREADS = 2;

int main()
{
	/* Auto Scaling */
	unsigned numOfcores = std::thread::hardware_concurrency();
	fprintf(stderr, "There are %d Cores Detected on This System \n", numOfcores);
	if (numOfcores)
		::SCALE_THREADS = numOfcores;
	fprintf(stderr, "\nStarting the batch jobs\n\n");

	fprintf(stderr, "\nExecution Times (in microseconds)\n");
	fprintf(stderr, "\n\tSize of the samples: %d | Scale threads: %d\n\n", NUM_OF_TRIALS, SCALE_THREADS);

	
	// Run the batch jobs
	batch(0, 0);
	batch(1, 1);
	batch(1, 2);
	batch(1, 3);

	return 0;
}

void batch(unsigned grey_Id, unsigned sobel_Id)
{
	//-------------------------------------------------------------------------------------------
	PPMImage *source;
	source = PPM::readPPM("model.ppm");

	PGMImage  *img_grey;
	img_grey = (PGMImage *)malloc(sizeof(PGMImage));
	img_grey->x = source->x;
	img_grey->y = source->y;
	img_grey->data = (unsigned char*)malloc(img_grey->x * img_grey->y * sizeof(unsigned char));
	//-------------------------------------------------------------------------------------------

	long time_measurements[NUM_OF_TRIALS];

	fprintf(stderr, "\n - - - - - - - - - - - - - - - - - - - - - - - - - -\n");

	switch (grey_Id)
	{
	case 0:
		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE

			serial_grey(source, img_grey);

			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("serial_grey.pgm", img_grey);

		fprintf(stderr, "\nConvert To Grey Scale (serial)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median(time_measurements, NUM_OF_TRIALS), STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));

		break;




	case 1:
		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE
			parallel_grey(source, img_grey);
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("parallel_grey.pgm", img_grey);

		fprintf(stderr, "\nConvert To Grey Scale (parallel)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median(time_measurements, NUM_OF_TRIALS), STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));
	}
	fprintf(stderr, "\n       - - - - - - - - - - - - - - - -              \n");

	//-------------------------------------------------------------------------------------------
	PGMImage  *img_sobel;
	img_sobel = (PGMImage *)malloc(sizeof(PGMImage));
	img_sobel->x = img_grey->x;
	img_sobel->y = img_grey->y;
	img_sobel->data = (unsigned char*)malloc(img_sobel->x * img_sobel->y * sizeof(unsigned char));
	//-------------------------------------------------------------------------------------------
	switch (sobel_Id)
	{

	case 0:

		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 

			serial_sobel(img_grey, img_sobel);

			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("serial_sobel.pgm", img_sobel);

		fprintf(stderr, "\nApply Sobel (serial)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median(time_measurements, NUM_OF_TRIALS), STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));

		break;


		

	case 1:

		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();

			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 

			parallel_sobel(img_grey, img_sobel);

			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 

			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("parallel_sobel.pgm", img_sobel);

		fprintf(stderr, "\nApply Sobel (parallel)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median(time_measurements, NUM_OF_TRIALS),   STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));

		break;




	case 2:

		for(unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 
		
			parallel_sobel_4boxes(img_grey, img_sobel);
		
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("parallel_sobel_4boxes.pgm", img_sobel);
		
		fprintf(stderr, "\nApply Sobel (parallel with fixed 4 boxes)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median  (time_measurements, NUM_OF_TRIALS), STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));
		
		break;




	case 3:

		for (unsigned i = 0; i < NUM_OF_TRIALS; i++)
		{
			auto start = std::chrono::high_resolution_clock::now();
			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 

			omp_sobel(img_grey, img_sobel);

			// TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE -- TIME SENSITIVE 
			auto elapsed = std::chrono::high_resolution_clock::now() - start;
			long long microsecs = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
			time_measurements[i] = microsecs;
		}
		PPM::writePGM("omp_sobel.pgm", img_sobel);

		fprintf(stderr, "\nApply Sobel (OpenMP)\n");
		fprintf(stderr, "\tMedian: %d | Mean: %d \n", STAT::median  (time_measurements, NUM_OF_TRIALS), STAT::mean(time_measurements, NUM_OF_TRIALS));
		fprintf(stderr, "\tVariance: %d | SD: %d \n", STAT::variance(time_measurements, NUM_OF_TRIALS), STAT::sd(time_measurements, NUM_OF_TRIALS));
		
	}
	fprintf(stderr, "\n - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
	
	free(source);
	free(img_grey);
	free(img_sobel);
}

void serial_grey(PPMImage *source, PGMImage  *img_grey)
{
	// Sorry Need to reduce branching to get those extra points
	//if ((source) && (img_grey))
	//{
		unsigned i;
		for (i = 0; i < img_grey->x*img_grey->y; i++)
		{
			img_grey->data[i] = (source->data[i].red*0.3 + source->data[i].green*0.59 + source->data[i].blue*0.11);
		}
	//}
}

void parallel_grey(PPMImage *source, PGMImage  *img_grey)
{
	// Sorry Need to reduce branching to get those extra points
	//if ((source) && (img_grey))
	std::thread *threads = new std::thread[SCALE_THREADS];
	for (unsigned i = 0; i < SCALE_THREADS; i++) {
		threads[i] = std::thread([&source, &img_grey,i]()
		{
			
			for (unsigned i2 = ((source->x*source->y) / SCALE_THREADS)*i; i2 < ((source->x*source->y) / SCALE_THREADS)*(i + 1); i2++)
			{
				img_grey->data[i2] = (source->data[i2].red*0.3 + source->data[i2].green*0.59 + source->data[i2].blue*0.11);
			}

		});
	}

	// Join
	for (unsigned i = 0; i < SCALE_THREADS; i++)
	{
		threads[i].join();
	}
}

void serial_sobel(PGMImage *img_grey, PGMImage *img_sobel)
{
	// Sorry Need to reduce branching to get those extra points
	//if (img_grey)
	unsigned x = img_sobel->x;
	unsigned y = img_sobel->y;
	
	// Form 2nd row to (y-1)th row
	unsigned i;
	for (i = x+1; i < (x*y)-x-1; i++)
	{
		// Apply valid padding
		// Skip the left edge of the image
		if (!(i%x)){
			img_sobel->data[i] = 0;
			continue;
		}

		//Skip the right edge of the image
		if (!((i + 1) % x)){
			img_sobel->data[i] = 0;
			continue;
		}

		img_sobel->data[i] = SOBEL_FILTER(i)
	}
}

void parallel_sobel(PGMImage *img_grey, PGMImage *img_sobel)
{
	unsigned i;
	unsigned x = img_grey->x;
	unsigned y = img_grey->y;
	
	unsigned scale_parallel_end   = 0;

	std::thread head_t;
	std::thread *threads = new std::thread[SCALE_THREADS];

	for (i = 0; i < SCALE_THREADS; i++) {
		// Find the index from wich the tasks are parallelizable
		if (!((   ( (x*y) / SCALE_THREADS )*i   ) > x))
		{
			threads[i] = std::thread([](){});
			if (((((x*y) / SCALE_THREADS)*(i + 1)) > x))
			{
				head_t = std::thread([&img_sobel, x, y, i, &img_grey]()
				{
					for (unsigned i2 = x + 1; i2 < (((x*y) / SCALE_THREADS)*(i + 1)); i2++)
					{
						// Apply valid padding
						// Skip the left edge of the image
						if (!(i2%x)){
							img_sobel->data[i2] = 0;
							continue;
						}

						//Skip the right edge of the image
						if (!((i2 + 1) % x)){
							img_sobel->data[i2] = 0;
							continue;
						}

						img_sobel->data[i2] = SOBEL_FILTER(i2)
					}
				});
			}
				
			continue;
		}

		if (     ( ((x*y) / SCALE_THREADS)*(i + 1) ) >= (x*y - x - 1)      )
		{
			threads[i] = std::thread([](){});
			scale_parallel_end = (((x*y) / SCALE_THREADS)*i);

			continue;
		}
		
		threads[i] = std::thread([&img_sobel, x, y, i, &img_grey]()
		{			
			unsigned i2;
			for (i2 = ((x*y) / SCALE_THREADS)*i; i2 < ((x*y) / SCALE_THREADS)*(i + 1); i2++)
			{
				// Apply valid padding
				// Skip the left edge of the image
				if (!(i2%x)){
					img_sobel->data[i2] = 0;
					continue;
				}

				//Skip the right edge of the image
				if (!((i2 + 1) % x)){
					img_sobel->data[i2] = 0;
					continue;
				}

				img_sobel->data[i2] = SOBEL_FILTER(i2)
			}
		});
	}

	for (i = scale_parallel_end; i < (x*y) - x - 1; i++)
	{
		// Apply valid padding
		// Skip the left edge of the image
		if (!(i%x)){
			img_sobel->data[i] = 0;
			continue;
		}
		//Skip the right edge of the image
		if (!((i + 1) % x)){
			img_sobel->data[i] = 0;
			continue;
		}
		img_sobel->data[i] = SOBEL_FILTER(i)
	}

	// Join
	head_t.join();
	for (i = 0; i < SCALE_THREADS - 1; i++)
	{
		threads[i].join();
	}
}

void parallel_sobel_4boxes(PGMImage *img_grey, PGMImage *img_sobel)
{
	unsigned x = img_grey->x;
	unsigned y = img_grey->y;

	std::thread *threads = new std::thread[4];

	threads[0] = std::thread([x,y, &img_sobel, &img_grey]()
	{
		// Occupy left upper box
		for (unsigned i = 1; i < y / 2; i++){
			for (unsigned i2 = x*i; i2 < x*i + (x / 2); i2++){
				// Apply valid padding
				// Skip the left edge of the image
				if (!(i2%x)){
					img_sobel->data[i2] = 0;
					continue;
				}

				img_sobel->data[i2] = SOBEL_FILTER(i2)
			}
		}
	});
	threads[1] = std::thread([x, y, &img_sobel, &img_grey]()
	{
		// Occupy right upper box
		for (unsigned i = 1; i < y / 2; i++){
			for (unsigned i2 = x*i + (x / 2); i2 < x*i + (x); i2++){
				// Apply valid padding
				//Skip the right edge of the image
				if (!((i2 + 1) % x)){
					img_sobel->data[i2] = 0;
					continue;
				}

				img_sobel->data[i2] = SOBEL_FILTER(i2)
			}
		}
	});
	threads[2] = std::thread([x, y, &img_sobel, &img_grey]()
	{
		// Occupy left lower box
		for (unsigned i = y / 2; i < y - 1; i++){
			for (unsigned i2 = x*i; i2 < x*i + (x / 2); i2++){
				// Apply valid padding
				// Skip the left edge of the image
				if (!(i2%x)){
					img_sobel->data[i2] = 0;
					continue;
				}

				img_sobel->data[i2] = SOBEL_FILTER(i2)
			}
		}
	});
	threads[3] = std::thread([x, y, &img_sobel, &img_grey]()
	{
		// Occupy right upper box
		for (unsigned i = y / 2; i < y - 1; i++){
			for (unsigned i2 = x*i + (x / 2); i2 < x*i + (x); i2++){
				// Apply valid padding
				//Skip the right edge of the image
				if (!((i2 + 1) % x)){
					img_sobel->data[i2] = 0;
					continue;
				}
				img_sobel->data[i2] = SOBEL_FILTER(i2)
			}
		}
	});

	// Join
	for (unsigned i = 0; i < 4; i++)
	{
		threads[i].join();
	}
}

void omp_sobel(PGMImage *img_grey, PGMImage *img_sobel)
{
	// Sorry Need to reduce branching to get those extra points
	//if (img_grey)
	unsigned x = img_sobel->x;
	unsigned y = img_sobel->y;

	// Form 2nd row to (y-1)th row

#pragma omp parallel for
	 for (int i = x + 1; i < (x*y) - x - 1; i++)
	{
		// Apply valid padding
		// Skip the left edge of the image
		if (!(i%x)){
			img_sobel->data[i] = 0;
			continue;
		}

		//Skip the right edge of the image
		if (!((i + 1) % x)){
			img_sobel->data[i] = 0;
			continue;
		}

		img_sobel->data[i] = SOBEL_FILTER(i)
	}
}