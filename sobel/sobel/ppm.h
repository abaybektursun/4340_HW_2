#ifndef PPM_H
#define PPM_H

#include <stdio.h>
#include <stdlib.h>

//---------------------------------//
typedef struct {				   //
	unsigned char red, green, blue;//
} PPMPixel;						   //
								   //
typedef struct {				   //
	int x, y;					   //
	PPMPixel *data;				   //
} PPMImage;						   //
//---------------------------------//


//---------------------------//
typedef struct {			 //
	int x, y;				 //
	unsigned char *data;     //
} PGMImage;			 //
//---------------------------//

#define RGB_COMPONENT_COLOR 255

static class PPM{
public:
	static PPMImage *readPPM(const char *filename)
	{
		char buff[16];
		PPMImage *img;
		FILE *fp;
		int c, rgb_comp_color;

		//open PPM file for reading
		fp = fopen(filename, "rb");//reading a binary file
		if (!fp) {
			fprintf(stderr, "Unable to open file '%s'\n", filename);
			exit(1);
		}
	
		//read image format
		if (!fgets(buff, sizeof(buff), fp)) {
			perror(filename);
			exit(1);
		}
	
		//check the image format can be P3 or P6. P3:data is in ASCII format    P6: data is in byte format
		if (buff[0] != 'P' || buff[1] != '6') {
			fprintf(stderr, "Invalid image format (must be 'P6')\n");
			exit(1);
		}
	
		//alloc memory form image
		img = (PPMImage *)malloc(sizeof(PPMImage));
		if (!img) {
			fprintf(stderr, "Unable to allocate memory\n");
			exit(1);
		}
	
		//check for comments
		c = getc(fp);
		while (c == '#') {
			while (getc(fp) != '\n');
			c = getc(fp);
		}
	
		ungetc(c, fp);//last character read was out back
		//read image size information
		if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {//if not reading width and height of image means if there is no 2 values
			fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
			exit(1);
		}
	
		//read rgb component
		if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
			fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
			exit(1);
		}
	
		//check rgb component depth
		if (rgb_comp_color != RGB_COMPONENT_COLOR) {
			fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
			exit(1);
		}
	
		while (fgetc(fp) != '\n');
		//memory allocation for pixel data for all pixels
		img->data = (PPMPixel*)malloc(img->x * img->y * sizeof(PPMPixel));
	
		if (!img) {
			fprintf(stderr, "Unable to allocate memory\n");
			exit(1);
		}
	
		//read pixel data from file
		if (fread(img->data, 3 * img->x, img->y, fp) != img->y) { //3 channels, RGB
			fprintf(stderr, "Error loading image '%s'\n", filename);
			exit(1);
		}
	
		fclose(fp);
		return img;
	}
	static void writePPM(const char *filename, PPMImage *img)
	{
		FILE *fp;
		//open file for output
		fp = fopen(filename, "wb");//writing in binary format
		if (!fp) {
			fprintf(stderr, "Unable to open file '%s'\n", filename);
			exit(1);
		}
	
		//write the header file
		//image format
		fprintf(fp, "P6\n");
	
		//image size
		fprintf(fp, "%d %d\n", img->x, img->y);
	
		// rgb component depth
		fprintf(fp, "%d\n", RGB_COMPONENT_COLOR);
	
		// pixel data
		fwrite(img->data, 3 * img->x, img->y, fp);
		fclose(fp);
	}
	
	
	void changeColorPPM(PPMImage *img)//Negating image
	{
		int i;
		if (img){
	
			for (i = 0; i<img->x*img->y; i++){
				img->data[i].red   = RGB_COMPONENT_COLOR - img->data[i].red;
				img->data[i].green = RGB_COMPONENT_COLOR - img->data[i].green;
				img->data[i].blue  = RGB_COMPONENT_COLOR - img->data[i].blue;
			}
		}
	}

	static void writePGM(const char *filename, PGMImage *img)
	{
		/*
		Type				Magic number	Magic number	Extension	Colors
		Portable BitMap		P1	ASCII		P4	binary		.pbm		0�1 (black & white)
		Portable GrayMap	P2	ASCII		P5	binary		.pgm		0�255 (gray scale)
		Portable PixMap		P3	ASCII		P6	binary		.ppm		0�255 (RGB)
		*/

		FILE *fp;
		//open file for output
		fp = fopen(filename, "wb");//writing in binary format
		if (!fp) {
			fprintf(stderr, "Unable to open file '%s'\n", filename);
			exit(1);
		}

		//write the header file
		//image format
		fprintf(fp, "P5\n");

		//image size
		fprintf(fp, "%d %d\n", img->x, img->y);

		// rgb component depth
		fprintf(fp, "%d\n", RGB_COMPONENT_COLOR);

		// pixel data
		fwrite(img->data, img->x, img->y, fp);
		fclose(fp);
	}
};

#endif